﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreData
{
    interface ILogger
    {
        public void CreateLoggerFile(string message, string location); 
    }
}

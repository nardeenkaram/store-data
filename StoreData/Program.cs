﻿using System;

namespace StoreData
{

    class Program
    {
        static void Main(string[] args)
        {
            GoodsHandler goodsHandler = new GoodsHandler();
            string[] csvLines = goodsHandler.ProcessFile(@"D:\\Store Data Task\\Store Data.csv");
            int startingIndex = goodsHandler.FindIndex(csvLines);
            if(startingIndex == -1)
            {
                Console.WriteLine("Can't find file header");
            }
            else
            {
                Console.WriteLine("starting index", startingIndex);
                for (int i = startingIndex + 1; i < csvLines.Length; i++)
                {
                    goodsHandler.HanldeNewLine(csvLines[i]);
                }
            }
            
        }
    }
}

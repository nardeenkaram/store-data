﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace StoreData
{
    class FileManager
    {
        private FileManager() { }
        private static FileManager fileManager = null;
        public static FileManager ManagerFile
        {
            get
            {
                if (fileManager == null)
                {
                    fileManager = new FileManager();
                }
                return fileManager;
            }
        }

        public void CreateFile(string good, string filePath)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(filePath, true))
                {
                    file.WriteLine(good);
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException("program failed with err: " + ex);
            }
        }
    }
}

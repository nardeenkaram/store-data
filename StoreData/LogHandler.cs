﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace StoreData
{
    class LogHandler : ILogger
    {
        private LogHandler() { }
        private static LogHandler logHandler = null;
        public static LogHandler HandlerLog
        {
            get
            {
                if (logHandler == null)
                {
                    logHandler = new LogHandler();
                }
                return logHandler;
            }
        }

        FileManager fileManager = FileManager.ManagerFile;
        public void CreateLoggerFile(string good, string filePath)
        {
            fileManager.CreateFile(good, filePath);
           
        }
    }
}

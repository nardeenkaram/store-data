﻿using System;
using System.Collections.Generic;
using System.Text;
using CsvHelper;
using System.IO;
using System.Globalization;
using System.Linq;
using CsvHelper.Configuration.Attributes;
using CsvHelper.Configuration;

namespace StoreData
{

    class GoodsHandler
    {
        public GoodsHandler()
        {
        }

        private readonly FileManager fileManager = FileManager.ManagerFile;
        private readonly LogHandler logHandler = LogHandler.HandlerLog;

        public int FindIndex(string[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                string header = "Good ID;Transaction ID;Transaction Date;Amount;Direction;Comments";

                if (array[i].Equals(header))
                {
                    return i;
                }
            }
            return -1; //header not found
        }

        public string[] ProcessFile(string fileLocation)
        {
            string[] csvLines = File.ReadAllLines(fileLocation);
            return csvLines;
        }


        public void HanldeNewLine(string newCsvLine)
        {
            bool isValid = true;
            // validate line
            var rowData = newCsvLine.Split(';');

            for (int i = 0; i < rowData.Length - 1; i++)
            {
                if (String.IsNullOrEmpty(rowData[i]))
                {
                    isValid = false;
                    logHandler.CreateLoggerFile(newCsvLine, "D:\\Store Data Task\\log.csv");
                }
            }
            if (isValid)
            {
                fileManager.CreateFile(newCsvLine, "D:\\Store Data Task\\Good-" + rowData[0] + ".csv");
            }

        }
    }
}
